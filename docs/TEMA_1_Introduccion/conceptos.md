Conceptos previos
=================

**LA ARQUITECTURA CLIENTE/SERVIDOR**

Es la estructura en la que se basa el funcionamiento de los servicios de red. Es un modelo de aplicación distribuida en el que las tareas se reparten entre los proveedores de recursos o servicios, llamados servidores, y los demandantes, llamados clientes. Un cliente realiza peticiones a otro programa, el servidor, que le da respuesta. **Especialmente adecuada para REDES de computadores.**

<a title="Alancaio / CC BY-SA (https://creativecommons.org/licenses/by-sa/4.0)" href="https://commons.wikimedia.org/wiki/File:Cliente-servidor.jpeg"><img width="256" alt="Cliente-servidor" src="https://upload.wikimedia.org/wikipedia/commons/6/69/Cliente-servidor.jpeg" style="display:block;margin-left:auto;margin-right:auto;"></a>
Las características de ambos tipos de equipos suelen ser distintas:
<table style=" margin-left: auto;margin-right: auto;padding:5px; border:1;">
    <thead>
        <tr>
            <th>Clientes</th>
            <th>Servidores</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td >
            <ul>
              <li>Menor potencia de cálculo</li>
              <li>Peticiones al srv.</li>
              <li>Menor potencia de cálculo</li>
              <li>Interactuar con varios srv.</li>
              <li>If visual+   Conectividad</li>
            </ul>
            </td>
            <td >
            <li>Mayor potencia de cálculo</li>
            <li>Permanece a la espera</li>
            <li>Gran número de peticiones</li>
            <li>Prioridad rendimiento</li>
            <li>Seguridad</li>
            </td>
        </tr>
    </tbody>
</table>

**INTERNET**

No es más que un conjunto de redes locales **interconectadas** entre sí:
+ **LAN vs WAN**
+ La conexión se realiza a través de dispositivos denominados *nodos*
+ Los servicios, en un principio, se ofrecen dentro de la red local.
+ Para hacerlos públicos en Internet han de realizarse las correspondientes configuraciones.

<a title="Mro / CC BY-SA (https://creativecommons.org/licenses/by-sa/3.0)" href="https://commons.wikimedia.org/wiki/File:Internet-transit.svg"><img width="512" alt="Internet-transit" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Internet-transit.svg/512px-Internet-transit.svg.png" style="display:block;margin-left:auto;margin-right:auto;"></a>

___
> **RECUERDA...**
1. ¿Que dispositivos  HW actuan típicamente como NODOS?
2. ¿Que Hardware necesitan obligatoriamente esos dispositivos?.
3. ¿Como se llama la accion de ”hacer visible” un servicio a traves de Internet?
4. ¿Podrías hacerlo en tu casa?
5. ¿Y la acción que permite a los equipos de una LAN navegar por Internet?
___
