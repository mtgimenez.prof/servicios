Introducción a los Servicios de Red
===================================
La finalidad de una red, bien sea local o de área extensa, es que los usurarios de los sistemas informáticos de una organización puedan hacer un mejor uso de los mismos mejorando de este modo el rendimiento global de la organización, medianta la utilización de servicios como:

    * Acceso remoto a archivos.
    * Correo Electrónico
    * Acceso a información de hipertexto(pag. Web)
    * Otros servicios(muchos de ellos transparentes para l@s usuari@s)

En este tema vamos a analizar los siguientes apartados, los cuales serán **básicos** para poder trabajar el resto del curso en los distintos servicios que aprenderemos a configurar.

.. toctree::
   :maxdepth: 2

   conceptos
   componentes
   virtualizacion
   escenario
