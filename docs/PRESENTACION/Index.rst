Presentación Módulo
===================

El módulo profesional **Servicios de Red e Internet** se imparte durante el segundo curso del Ciclo Formativo de Grado Superior de *Administración de Sistemas Informáticos en Red* (Puedes encontrar más información en el `dossier del ciclo en la web de Conselleria <http://www.ceice.gva.es/es/web/formacion-profesional/publicador-ciclos/-/asset_publisher/FRACVC0hANWa/content/ciclo-formativo-administracion-de-sistemas-informaticos-en-red>`_.

.. list-table:: **CURSO 2024-2025**
   :widths: auto
   :header-rows: 0
   :align: center

   * - Tomás Giménez Albert
   * - `mt.gimenezalbert@edu.gva.es <mailto:mt.gimenezalbert@edu.gva.es/>`_
   * - *Lunes 15:30 – 18:09 | Viernes 18:41 – 21:20*
   * - **6 H. Sem * 20 sem = 120 HORAS**

.. toctree::
   :maxdepth: 4

   presentacion
