Calendario del curso
===============
![](img/CalendariBotet24-25_CCFF.png "Calendario Botet Curso 2024-25")

Objetivos del módulo
===============
+ **Gestiona entornos de virtualización.**
+ Administra servicios de **resolución de nombres**, analizándolos y garantizando la seguridad del servicio.
+ Administra servicios de **configuración automática**, identificándolos y verificando la correcta asignación de los parámetros.
+ Administra **servidores Web** aplicando criterios de configuración y asegurando el funcionamiento del servicio.
+ Administra servicios de **transferencia de archivos** asegurando y limitando el acceso a la información.
+ Administra servidores de **correo electrónico**, aplicando criterios de configuración y garantizando la seguridad del servicio.
+ **Configura y administra escenarios en entornos de cloud computing.**
+ Administra servicios de mensajería instantánea, noticias y listas de distribución, verificando y asegurando el acceso de los usuarios.
+ Administra servicios de audio identificando las necesidades de distribución y adaptando los formatos.
+ Administra servicios de vídeo identificando las necesidades de distribución y adaptando los formatos.


Contenidos
===============

<table style=" margin-left: auto;margin-right: auto; border:1px solid;">
    <thead>
        <tr>
            <th>1ª Evaluación</th>
            <th>2ª Evaluación</th>
        </tr>
    </thead>
    <tbody style="border:1px solid;">
        <tr style="border:1px solid;">
            <td >Introducción a los Srvs. de red </td>
            <td >Interconexión de redes. Seguridad.</td>
        </tr>
        <tr style="border:1px solid;">
        <td>Asignación dinámmica. DHCP</td>
        <td>Servcios de acceso remoto.</td>
        </tr>
        <tr style="border:1px solid;">
            <td >Sist. Nombres de dominio. DNS&nbsp;&nbsp;</td>
            <td>Servicio de Correo Electrónico</td>
        </tr>
        <tr style="border:1px solid;">
        <td >Servidores web</td>
        <td>Otros servicios</td>
        </tr>
    </tbody>
</table>

Funcionamiento
===============

+ Asistencia **PRESENCIAL**.
+ Si algo no funciona. Se cambia. Se habla...
+ Ventaja: vamos a usar **herramientas que ya conocemos** y que no nos deben ser complicadas de usar. Además debemos ser capaces de usar cada una correctamente.
  + WebFamilia -> Debéis tener acceso. Si no es así solicitarlo.
  + Aules. → Materiales, tareas, cuestionarios, *foros, chats, ...*
  + Correo **@alu.edu.gva.es** que incluye herramientas como:
    + **Onedrive** → Almacenamiento en la nube.
    + Teams → Videoconferencia.
    + **Calendario** -> Tendremos calendario de grupo para exámenes, controles, etc...y además podéis tener vuestro propio calendario.
+ + Telegram (grupo para noticias/anuncios) → [Enlace al canal](https://t.me/+zR3rCJqs-I0xM2U8)

  ![](img/QRGrupoTelegram.png "Enlace al grupo de Telegram")

+ Las explicaciones teóricas se harán en clase con lo más reducidas posible y a trabajar (**en clase y en casa**).
+ A través de los medios anteriores o en clase se podrán resolver las dudas que vayan apareciendo.
+ Los puestos están señalados. NO desplazar equipo o mobiliario.
+ Siempre nos sentaremos en el mismo sitio
+ Para evitar las pérdidas de información y facilitar tu trabajo tienes dos opciones (**MUY RECOMENDABLE**):
  + Usa tu propio disco duro externo/portátil SSD (seguro que será más rápido que el DD del PC del aula).
  + Traer tu propio portátil (En este caso habría que ubicarte en algún puesto concreto. **ATENCIÓN AL USO DE LOS CABLES DE RED**).
+ **Atención con la UBICACIÓN de tus MV si están en el DD del PC.**
+ **Estrenamos PDIs. NO TOCAR**

Evaluación
===========

+ Prácticas / Ejercicios=40%
+ Exámenes por tema/s=30%
+ Examen Final Evaluación=30%
+ Aprobar una evaluación significa superar esa parte del módulo. En caso contrario se debe recuperar en la siguiente evaluación.
