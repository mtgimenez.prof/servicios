=====================
Servicio WEB. HTTP(S)
=====================

**Hypertext Transfer Protocol o HTTP (protocolo de transferencia de hipertexto)** es el protocolo usado en cada transacción de la World Wide Web.
HTTP  define la sintaxis y la semántica que utilizan los elementos de software de la arquitectura web (clientes, servidores, proxies) para comunicarse.
Es un **protocolo orientado a conexión** y sigue el esquema petición-respuesta entre un cliente y un servidor.

.. raw:: html

    <a title="Satishgavali at English Wikibooks, CC BY-SA 2.5 &lt;https://creativecommons.org/licenses/by-sa/2.5&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Prj1.jpeg"><img width="350" style="display:block;margin-left:auto;margin-right:auto;" alt="Prj1" src="https://upload.wikimedia.org/wikipedia/commons/7/78/Prj1.jpeg"></a>

Internet se creó en el CERN, pero su desarrollo lo **gestiona W3C (World Wide Web Consortium)**, quien se encarga de, entre otras cosas de los estándares que se van aplicando en la WWW.

          .. tip::

             * **Estándares** W3C.
             * **W3C Validation**
             * **W3C Schools**

Para estudiar este servicio, posiblemente el más importante del módulo de Servicios de Red **e Internet**, vamos a analizar estos apartados:

.. toctree::
   :maxdepth: 2

   componentes
   clientes
   http
   https
   rendimiento
   disponibilidad
