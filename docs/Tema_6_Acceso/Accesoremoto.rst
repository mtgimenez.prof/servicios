Acceso remoto
=============

El acceso remoto es una herramienta fundamental , tanto para los administradores, como para los usuarios finales de un sistema. El concepto de acceso remoto se refiere a la tecnología que permite a los servidores o equipos desde otros dispositivos. De forma simplificada, permitir el acceso remoto significa:
 1. Que los empleados pueden acceder a datos, correos electrónicos y otros documentos a través de cualquier dispositivo conectado a la LAN  o a Internet. 
 2. Posibilita que el **soporte técnico  de una empresa manipule una máquina y solucione un problema sin estar presente en el mismo lugar**. 

En el primer apartado estaríamos hablando, sobretodo, de las VPN (Virtual Private Network), las cuales son objetos de estudio en otro módulo de este ciclo. Puedes encontrar información en https://es.wikipedia.org/wiki/Red_privada_virtual. 

En el segundo caso, en el que nos vamos a centrar como sysadmins hablaríamos de servicios como SSH, RDP...
En todo caso estamos hablando de configuraciones muy úitles para los sistemas informáticos pero que, por sus propias características debemos saber configurar muy bien para no generar agujeros de seguridad.

SSH
----



RDP
---

VNC
----
